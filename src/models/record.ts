import { Schema, model } from "mongoose";

export interface IRecord {
	content: string;
}

export const recordSchema = new Schema<IRecord>({
	content: { type: String, required: true },
}).set("toJSON", {
	virtuals: true,
	versionKey: false,
	transform: (doc, ret) => { delete ret._id; },
});

export const Record = model<IRecord>("Record", recordSchema);
