import { connect, Document } from "mongoose";

import { IRecord, Record } from "../models/record";

export class RecordService {

	public async connect(mongoUrl: string): Promise<void> {
		await connect(mongoUrl);
	}

	public async create(input: IRecord): Promise<IRecord> {
		const record: Document = new Record(input);

		await record.save();

		return <IRecord>record.toJSON();
	}

	/*
		Retrieves all records ordered by store-time
	*/
	public async retrieve(): Promise<IRecord[]> {
		return (await Record.find({})).map((r: Document) => <IRecord>r.toJSON());
	}

}

export const recordService: RecordService = new RecordService();
