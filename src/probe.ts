import * as net from "net";
import { EventEmitter } from "events";

export class Probe extends EventEmitter {

	readonly port: number;
	private _up: boolean;
	get up(): boolean { return this._up; }
	get down(): boolean { return !this._up; }
	private _upRequest: boolean;
	get upRequest(): boolean { return this._upRequest; }
	private _downRequest: boolean;
	get downRequest(): boolean { return this._downRequest; }

	private server: net.Server = net.createServer();

	constructor(port: number) {
		super();
		this.port = port;

		this.server.on("connection", (socket: net.Socket) => {});

		this.server.on("error", (error: Error) => {
			this._up = false;
			this.emit("down");
			this.emit("error", error);
			this._upRequest = false;
			this._downRequest = false;
			this.doDown();
		});
		this.server.on("listening", () => {
			this._up = true;
			this.emit("up");
			this._upRequest = false;
		});
		this.server.on("close", () => {
			this._up = false;
			this.emit("down");
			this._downRequest = false;
		});
	}

	private doUp(): void {
		this.server.listen(this.port);
	}

	private doDown(): void {
		this.server.close();
	}

	public setUp(): void {
		this._downRequest = false;
		this._upRequest = true;
		this.emit("uprequest");
		
		this.doUp();
	}

	public setDown(): void {
		this._upRequest = false;
		this._downRequest = true;

		this.doDown();
	}

}
