import { IRecord } from "../models/record";
export declare class RecordService {
    connect(mongoUrl: string): Promise<void>;
    create(input: IRecord): Promise<IRecord>;
    retrieve(): Promise<IRecord[]>;
}
export declare const recordService: RecordService;
