/// <reference types="node" />
import { EventEmitter } from "events";
export declare class Probe extends EventEmitter {
    readonly port: number;
    private _up;
    get up(): boolean;
    get down(): boolean;
    private _upRequest;
    get upRequest(): boolean;
    private _downRequest;
    get downRequest(): boolean;
    private server;
    constructor(port: number);
    private doUp;
    private doDown;
    setUp(): void;
    setDown(): void;
}
