/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/schemaoptions" />
import { Schema } from "mongoose";
export interface IRecord {
    content: string;
}
export declare const recordSchema: Schema<IRecord, import("mongoose").Model<IRecord, any, any, any>, {}, {}>;
export declare const Record: import("mongoose").Model<IRecord, {}, {}, {}>;
